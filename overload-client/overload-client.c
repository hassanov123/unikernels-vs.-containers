
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define PORT 8123
#define SERVER_IP "172."

void harrassment(){
    FILE* fp;
        
    int server_reply;
    char comma =  ',';
    struct sockaddr_in server;
    int sock, one = 1;
    
    fp = fopen("started_clients.txt", "a");
    
        //Create socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock == -1){
        printf("Could not create socket");
    }
    puts("Socket created");

    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);
    server.sin_addr.s_addr = inet_addr(SERVER_IP);

    // Connect to server
    if(connect(sock, (struct sockaddr*)&server, sizeof(server)) < 0){
        perror("Connection failed. Error");
    }
    puts("Connected\n");

    if(send(sock, &one, one*sizeof(int*), 0) < 0){
        puts("Send failed");
    }

    //Receive a reply from server
    if(recv(sock, &server_reply, one*sizeof(int), 0) < 0){
        puts("recv failed");
    }
    if(server_reply == 1)
        fprintf(fp, "%d" "%c", server_reply, comma);
        
        
    //close socket
    close(sock);
    
    fclose(fp);

} 

int main(){      
    harrassment();

   return 0;
}
