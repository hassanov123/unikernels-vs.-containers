#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define SERVER_PORT 8123
#define SIZE 1000

void swap(int *xp, int *yp)	{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// A function to implement bubble sort
void bubbleSort(int arr[], int size)	{
    for(int i = 0; i < size - 1; i++)
		// Last i elements are already in place
		for (int j = 0; j < size - i-1; j++)
			if (arr[j] > arr[j+1])
				swap(&arr[j], &arr[j+1]);
}

// A function to implement reversed bubble sort
void reverseBubbleSort(int arr[], int size)	{
	for(int i = 0; i < size - 1; i++)
		// Last i elements are already in place
		for (int j = 0; j < size - i-1; j++)
			if (arr[j] < arr[j+1])
				swap(&arr[j], &arr[j+1]);
}

int acceptIncomingRequests()	{

	int serverSocket, newSocket;
	socklen_t addr_size;
	struct sockaddr_in serverAddr;
	struct sockaddr_storage serverStorage;

	//Create the socket and configure settings of the server address struct
	serverSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (serverSocket < 0) {
		printf("Failed to create socket!\n");
		return 0;
	}

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;

	int rc = bind(serverSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
	if(rc < 0)	{
		printf("Failed to bind socket!");
		return 0;
	}

	//Listen on the socket, with 1000 max connection requests queued
	if(listen(serverSocket, 1000) == 0)
		printf("Listening on port %d\n", SERVER_PORT);
	else	{
		printf("Error listening to socket!\n");
		return 0;
	}

	while(1)	{
		//printf("Waiting for incoming request... \n");
		//Accept call creates a new socket for the incoming connection
		addr_size = sizeof(serverStorage);
		newSocket = accept(serverSocket, (struct sockaddr *) &serverStorage, &addr_size);
		if(newSocket < 0)
			printf("Failed to accept incoming request!\n");
		else	{
			int one = 1;
			printf("Request accepted");
			send(newSocket, &one, sizeof(int*), 0);
			
			int array[SIZE];
			for (int i = 0; i < SIZE; i++)
				array[i] = i;
				
			while(1){
					reverseBubbleSort(array, SIZE);
					bubbleSort(array, SIZE);
			}
			return 1;
		}
		return 0;
	}
}

int main(){
	acceptIncomingRequests();
	    
   return 0;
}
