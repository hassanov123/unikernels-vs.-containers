#!/usr/bin/env python3
import os
import time
import psutil

# Client commands
clientCompileLine = "gcc -o overloadClient overload-client.c"
clientStartLine = "./overloadClient"

# Server commands
serverBuildDockerLine = "sudo docker build -t overload ."
serverStartLine = "sudo docker run -p 8123:8123 --rm --name overload overload &"
serverStopLine = "sudo docker kill overload"
killAllContainers = "pidof containerd-shim | sudo xargs kill"

#print systemwide stats
def stats(f, text):
	f.write(text + "\n")
	f.write("CPU %: " + str(psutil.cpu_percent()) + "\n")
	f.write("Used memory: " + str(psutil.virtual_memory()[3]) + "\n")
	f.write("Active memory: " + str(psutil.virtual_memory()[5]) + "\n")
	f.write('Memory used %: ' + str(psutil.virtual_memory()[2]) + "\n\n")

	print(text)
	print("Used memory: " + str(psutil.virtual_memory()[3]))
	print("Active memory: " + str(psutil.virtual_memory()[5]))
	print("Memory used %: " + str(psutil.virtual_memory()[2]))

def main():
	iteration = 100
	f = open("runStats.txt", "a+")
	
	#configure and build server and client
	os.system(serverBuildDockerLine)
	os.system(clientCompileLine)
	
	time.sleep(2)
	f.write("-------------Test for " + str(iteration) + " iterations in Docker container--------\n")
	stats(f, "Before starting:")

	while(iteration > 0):
		serverPort = 8123 + iteration
		#start server
		os.system("sudo docker run -p " + str(serverPort) + ":8123 -d --rm --name overload" + str(iteration) + " overload")

		#start client
		os.system("./overloadClient " + str(serverPort))
		iteration -= 1
		print("iterations left: " + str(iteration))

	print("All iterations are done!");

	stats(f, "After all containers have been started:")
	time.sleep(15)
	stats(f, "After 15s and before stopping all:")
	os.system(killAllContainers)
	time.sleep(5)
	stats(f, "After 5s after all containers killed:")
	time.sleep(5)
	stats(f, "After 10s after all containers killed:")
	print("Program done!")

main()


