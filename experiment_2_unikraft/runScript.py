#!/usr/bin/env python3
import os
import time
import psutil

# Server commands
serverConfigure = "sudo kraft configure"
serverBuild = "sudo kraft build -j"
serverSetupNetwork = "sudo kraft-net --with-dnsmasq up"
serverStart = "sudo /usr/local/bin/qemu-guest -k build/experiment_2_unikraft_kvm-x86_64 -b net0 -t x86pc -x"
serverCloseNetwork = "sudo kraft-net --with-dnsmasq down"
killAllUnikernels = "pidof /usr/bin/qemu-system-x86_64 | sudo xargs kill"

# Client commands
clientCompile = "gcc -o overloadClient overload-client.c"
clientStart = "./overloadClient"

#print systemwide stats
def stats(f, text):
	f.write(text + "\n")
	f.write("CPU %: " + str(psutil.cpu_percent()) + "\n")
	f.write("Used memory: " + str(psutil.virtual_memory()[3]) + "\n")
	f.write("Active memory: " + str(psutil.virtual_memory()[5]) + "\n")
	f.write('Memory used %: ' + str(psutil.virtual_memory()[2]) + "\n\n")

	print(text)
	print("Used memory: " + str(psutil.virtual_memory()[3]))
	print("Active memory: " + str(psutil.virtual_memory()[5]))
	print("Memory used %: " + str(psutil.virtual_memory()[2]))

def main():
	iteration = 100

	f = open("runStats.txt", "a+")

	#configure and build the unikernel
	os.system(serverConfigure)
	os.system(serverBuild)
	os.system(serverSetupNetwork)

	#Compile the client
	os.system(clientCompile)
	
	time.sleep(2)
	f.write("-------------Test for " + str(iteration) + " iterations in Unikraft unikernel with 1000 long array--------\n")
	stats(f, "Before starting:")

	while(iteration > 0):
		#Start the server
		os.system(serverStart)
		os.system(clientStart)
		iteration -= 1
		print("iterations left: " + str(iteration))

	print("All iterations are done!");

	stats(f, "After all unikernels have been started:")
	time.sleep(15)
	stats(f, "After 15s and before stopping all:")
	os.system(killAllUnikernels)
	os.system(serverCloseNetwork)
	time.sleep(5)
	stats(f, "After 5s after all unikernels closed:")
	time.sleep(5)
	stats(f, "After 10s after all unikernels closed:")
	print("Program done!")

main()
	
