/*
 ============================================================================
 Name        : C-docker-server.c
 Author      : Hassan & Victor
 Version     :
 Copyright   : Free to use and distribute
 Description : to run this program in docker type the following:
 > docker build -t c-docker-server .
 > docker run -it --rm --name c-docker-server c-docker-server
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

#define LISTEN_IP "localhost"
#define LISTEN_PORT 8123

#define BUFLEN 2048
int recvbuf[BUFLEN];
int *inmsg;

void merge(int arr[], int l, int m, int r)	{
    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;

    /* create temp arrays */
    int L[n1], R[n2];

    /* Copy data to temp arrays L[] and R[] */
    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];

    // Merge the temp arrays back into arr[l..r]
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2)	{
        if (L[i] <= R[j])	{
            arr[k] = L[i];
            i++;
        }	else	{
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    // Copy the remaining elements of L[], if there are any
    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    // Copy the remaining elements of R[], if there are any
    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}

// l is for left index and r is right index of the sub-array of arr to be sorted
void mergeSort(int arr[], int l, int r) {
    if (l < r) {
        // Same as (l+r)/2, but avoids overflow for large l and h
        int m = l+(r-l)/2;

        // Sort first and second halves
        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);

        merge(arr, l, m, r);
    }
}

void printArray(int arr[], int size)	{
	for(int i = 0; i < size; i++)
		printf("%d ", arr[i]);
	printf("\n");
}

void mergeArr(int arr[], int arr_size)	{
	printf("in array is with size: %d \n", arr_size);
	//printArray(arr, arr_size);

	mergeSort(arr, 0, arr_size - 1);

	printf("Sorted array is with size: %d \n", arr_size);
	//printArray(arr, arr_size);
}

int main() {
	int rc = 0;
	int srv, client, read_size, total_size = 0, numberOfIterations;
	struct sockaddr_in srv_addr;

	srv = socket(AF_INET, SOCK_STREAM, 0);
	if (srv < 0) {
		fprintf(stderr, "Failed to create socket: %d\n", errno);
		goto out;
	}

	srv_addr.sin_family = AF_INET;
	srv_addr.sin_addr.s_addr = INADDR_ANY;
	srv_addr.sin_port = htons(LISTEN_PORT);

	rc = bind(srv, (struct sockaddr *) &srv_addr, sizeof(srv_addr));
	if (rc < 0) {
		fprintf(stderr, "Failed to bind socket: %d\n", errno);
		goto out;
	}

	/* Accept one simultaneous connection */
	rc = listen(srv, 10);

	if (rc < 0) {
		printf("Failed to listen on socket: %d\n", errno);
		goto out;
	}	else
		printf("Listening on port %d...\n", LISTEN_PORT);

	client = accept(srv, NULL, 0);
	if (client < 0) {
		printf("Failed to accept incoming connection: %d\n", errno);
		goto out;
	}	else
		printf("Connected! Waiting for incoming data... \n");

	if(recv(client, &numberOfIterations, sizeof(int), 0) < 0){
		printf("RECV FAILED - didn't count this run \n");
		return 0;
	}	else
		printf("Received numberOfIterations: %d\n", numberOfIterations);

	for(int i = 0; i < numberOfIterations; i++)	{

		while ((read_size = recv(client, &recvbuf, BUFLEN, 0)) > 0) {
			inmsg = (int *) malloc(read_size * 2);
			memcpy(inmsg, recvbuf, read_size);
			total_size += read_size;

			if(read_size < BUFLEN && inmsg[(total_size/sizeof(int))-1] == 1)
				goto done;

			while((read_size = recv(client, &recvbuf, BUFLEN, 0)) <= BUFLEN)	{
				inmsg = (int *) realloc(inmsg, (total_size + read_size) * 2);
				memcpy(inmsg + (total_size/sizeof(int)), recvbuf, read_size);
				total_size += read_size;

				if(inmsg[(total_size/sizeof(int))-1] == 1)	{
					done:
					mergeArr(inmsg, total_size/sizeof(int));

					/* Send reply */
					if (write(client, &inmsg[(total_size/sizeof(int))-1], sizeof(int)) < 0)
						printf("Failed to send a reply\n");
					else
						printf("Sent a reply %d\n", inmsg[(total_size/sizeof(int))-1]);

					read_size = 0;
					total_size = 0;
					free(inmsg);
					goto loopComplete;
				}
			}
		}

		if (read_size == 0)
			printf("Client disconnected!\n");
		else if (read_size == -1)
			printf("Receive failed!\n");

		loopComplete:
		printf("%d iterations left!\n", numberOfIterations-i-1);
	}

	/* Close connection */
	close(client);
	printf("Connection Closed! \n\n");

	out:
	return 0;
}
