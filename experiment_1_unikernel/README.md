# Experiment 1 Unikernel

För att kunna köra detta experiment se till att `Python3` och `Kraft`, kontrollera att `Kraft` finns installerad genom att köra 

    Kraft list

För att installera Kraft kör detta skript nedan, annars se Unikraft Docs:
    
    git clone https://github.com/unikraft/kraft.git
    cd kraft
    python3 setup.py install

For more information about `kraft` type ```kraft -h``` or read the
[documentation](http://docs.unikraft.org).

För att köra programmet starta först server-skriptet sedan client-skriptet i OLIKA terminaler

    python3 serverRunScript.py
    Python3 clientRunScript.py