#!/usr/bin/env python3
import os

# Client commands
clientCompileLine = "gcc -o arraySenderClient arraysender-client.c"
clientStartLine = "./arraySenderClient"

#Compile the client
os.system(clientCompileLine)

#start client
os.system(clientStartLine)
