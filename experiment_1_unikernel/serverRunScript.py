import os
import subprocess

# Server commands
serverConfigureLine = "sudo kraft configure"
serverBuildLine = "kraft build -j"
serverStartLine = "kraft run -p kvm -m x86_64 -s 1 -c 1 -M 4096"

#configure and build the unikernel
os.system(serverConfigureLine)
os.system(serverBuildLine)

#Start the server
os.system(serverStartLine)
print("Python script done")
