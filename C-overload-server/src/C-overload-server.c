/*
 ============================================================================
 Name        : C-overload-server.c
 Author      : Hassan Albaaj & Victor Berggren
 Version     :
 Copyright   : Free to use and distribute
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

//Necessary libraries
#include <stdio.h>
#include <stdlib.h>
#include "pthread.h"
#include <limits.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

//Testing
#include <sys/types.h>
#include <sys/syscall.h>

//Defined values
#define MAXTHREADS 1000000
#define THREADSTACK  65536
#define SERVER_PORT 8123

void createArray(int arr[], int size)	{
	for(int i = 0; i < size; i++)
		arr[i] = size - i;
}

void swap(int *xp, int *yp)	{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// A function to implement bubble sort
void bubbleSort(int arr[], int size)	{
    for(int i = 0; i < size - 1; i++)
		// Last i elements are already in place
		for (int j = 0; j < size - i-1; j++)
			if (arr[j] > arr[j+1])
				swap(&arr[j], &arr[j+1]);
}

// A function to implement reversed bubble sort
void reverseBubbleSort(int arr[], int size)	{
	for(int i = 0; i < size - 1; i++)
		// Last i elements are already in place
		for (int j = 0; j < size - i-1; j++)
			if (arr[j] < arr[j+1])
				swap(&arr[j], &arr[j+1]);
}

// A function to print an array
void printArray(int arr[], int size)	{
    for(int i = 0; i < size; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

void acknowledgeRequest(int socket)	{
	#define BUFLEN 2048
	int recvbuf[BUFLEN], one = 1;

	// receive a message
	if(recv(socket, &recvbuf, BUFLEN, 0) < 0)
		printf("Failed to receive a message!\n");

	// Acknowledge with a reply
	if (write(socket, &one, one * sizeof(int *)) < 0)
		printf("Failed to send a reply\n");
}

// A function to do work until crashing
void *doWork(void *arg)	{
	int size = *((int *)arg);
	int arr[size];

	pid_t tid = syscall(__NR_gettid);
	printf("# Starting thread %d \n", tid);

	createArray(arr, size);

	for(int i = 0; i < INT_MAX; i++)	{
		if(i % 2 == 0)	{
			bubbleSort(arr, size);
			//printf(" *** BubbleSorted for thread: %d", tid);
			//sleep(1);
		}	else
			reverseBubbleSort(arr, size);
		//printArray(arr, size);
	}
}

void createThread(pthread_t tid[], int socket, int threadNumber, int size)	{
	acknowledgeRequest(socket);

	if(pthread_create(&tid[threadNumber], NULL, doWork, &size) != 0)	{
		printf("FAILED TO CREATE THREAD!");
		return;
	}
}

void *acceptIncomingRequests(void *arg)	{
	pthread_t *tid = ((pthread_t *)arg);

	int serverSocket, newSocket, threadNumber = 1, size = 100;
	socklen_t addr_size;
	struct sockaddr_in serverAddr;
	struct sockaddr_storage serverStorage;

	//Create the socket and configure settings of the server address struct
	serverSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (serverSocket < 0) {
		printf("Failed to create socket!\n");
		return 0;
	}

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = INADDR_ANY;

	int rc = bind(serverSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
	if(rc < 0)	{
		printf("Failed to bind socket!");
		return 0;
	}

	//Listen on the socket, with 1000 max connection requests queued
	if(listen(serverSocket, 1000) == 0)
		printf("Listening on port %d\n", SERVER_PORT);
	else	{
		printf("Error listening to socket!\n");
		return 0;
	}

	while(1)	{
		//printf("Waiting for incoming request... \n");
		//Accept call creates a new socket for the incoming connection
		addr_size = sizeof(serverStorage);
		newSocket = accept(serverSocket, (struct sockaddr *) &serverStorage, &addr_size);
		if(newSocket < 0)
			printf("Failed to accept incoming request!\n");
		else	{
			printf(" - Creating Thread: %d!\n", threadNumber);
			createThread(tid, newSocket, threadNumber++, size);
		}
	}
}

int main(void) {
	pthread_t tid[MAXTHREADS];
	pthread_attr_t attrs;

	pthread_attr_init(&attrs);
	pthread_attr_setstacksize(&attrs, THREADSTACK);

	if(pthread_create(&tid[0], NULL, acceptIncomingRequests, &tid) != 0)	{
		printf("FAILED TO CREATE THREAD!");
		return 0;
	}	else
		printf("Created Thread\n");

	pthread_join(tid[0], NULL);

	//Close the server
	pthread_attr_destroy(&attrs);
	return EXIT_SUCCESS;
}
