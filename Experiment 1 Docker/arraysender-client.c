#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 8123
//#define SERVER_IP "172.88.0.76"
#define SERVER_IP "127.0.0.1"

static double timeStamp(void){
    struct timeval t;
    double oneMillion = 1000000;
    gettimeofday(&t, NULL);
    return (double)t.tv_sec + t.tv_usec/oneMillion;
}

double averageTime(double timeArr[])	{
	int arrSize = sizeof(timeArr);
	long double totalTime = 0;

	for(int i = 0; i < arrSize; i++)
		totalTime += timeArr[i];

	return totalTime/(long double)arrSize;
}

void printArray(int arr[], int size)	{
	for(int i = 0; i < size; i++)
		printf("%d ", arr[i]);
	printf("\n");
}

int main()	{
	FILE *fp;
	char comma = ',';
    double timeStart, timeStop;
    struct sockaddr_in server;
    int numOfIterations, 
        server_reply,
        sizeOfArray,
        sock;

    fp = fopen("measurements.txt", "a");

    printf("How large would you like the array?\n");
    scanf("%d", &sizeOfArray);
    
    printf("How many times?\n");
    scanf("%d", &numOfIterations);

    double timeArr[numOfIterations];

    printf("Size of array: %d, number of times sent: %d \n", sizeOfArray, numOfIterations);
 
    int values[sizeOfArray];

    for(int x = 0; x < sizeOfArray; x++)
        values[x] = sizeOfArray-x;

    //Create socket
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock == -1)	{
		perror("Could not create socket");
		return 0;
	}	else
		puts("Socket created");

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr(SERVER_IP);
	server.sin_port = htons(PORT);

	// Connect to server
	if((connect(sock, (struct sockaddr*)&server, sizeof(server))) < 0)	{
		printf("Connection failed. Error");
		return 0;
	}	else
		printf("Connected\n");

	if((send(sock, &numOfIterations, sizeof(int), 0)) < 0)	{
		printf("SEND FAILED \n");
		return 0;
	}	else
		printf("Sent numOfIterations %d successfully!\n", numOfIterations);

    for(int i = 0; i < numOfIterations; i++)	{
		timeStart = timeStamp();

		if((send(sock, &values, sizeOfArray * sizeof(int), 0)) < 0){
			printf("SEND FAILED \n");
			return 0;
		}

		//Receive a reply from server
		if(recv(sock, &server_reply, sizeOfArray * sizeof(int), 0) < 0){
			printf("RECV FAILED - didn't count this run \n");
			return 0;
		}

		timeArr[i] = timeStamp() - timeStart;
		printf("Elapsed time until server responded: %lf \tServer reply: %d\n", timeArr[i], server_reply);

		if(timeArr[i] < 1)
			fprintf(fp, "%lf" "%c", timeArr[i], comma);
    }

    //close socket'
	printf("Connection closed!\n");
	close(sock);
	fclose(fp);

    printf("Average time of all request sent is: %lf", averageTime(timeArr));

    return 0;
}
