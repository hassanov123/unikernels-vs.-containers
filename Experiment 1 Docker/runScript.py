#!/usr/bin/env python3
import os
import time

# Client commands
clientCompileLine = "gcc -o arraySenderClient arraysender-client.c"
clientStartLine = "./arraySenderClient"

# Server commands
serverBuildDockerLine = "sudo docker build -t arraysort ."
serverStartLine = "sudo docker run -p 8123:8123 -d -it --rm --name arraysort arraysort"
serverStopLine = "sudo docker kill arraysort"

os.system(serverBuildDockerLine)
os.system(clientCompileLine)

#start server
os.system(serverStartLine)
time.sleep(3)

#start client
os.system(clientStartLine)

#Server and client stops automatically

print('Done!')
